---
title: Overview
permalink: /
layout: default

nav_order: 1
---

# Developer Documentation
{: .fs-9 }

Are you a developer, designer or writer and want to help make VLC better? Then 
this is the right place for you to learn how to get started contributing to VLC.
{: .fs-6 .fw-300 }

## Contribution areas

There are different areas where you can contribute, here you can find a quick overview of these.
{: .fs-5 .fw-300 }

### Lua scripting
VLC can be extended with Lua scripts, Lua scripts can be used for _service discovery_,
_playlist_, _metadata fetcher_ or _interface_ modules. Additionally it is possible
to develop VLC _extensions_ with it.

### C/C++ modules
Most of VLC is written in _C_ and some modules in _C++_, if you are familiar
with those languages, check out this section for more information.

### Design/Write
Even as non-coder, you can still contribute to VLC by helping with design for the
VLC interface modules or the port of VLC. Alternatively you can help by writing
or improving the documentation.

### VLC Ports
VLC has ports for Android, iOS and WinRT, if you are familiar with any of those
and want to contribute, this is the place to get started.

### Web Developers
If you are a web developer you can contribute to improve the VLC Web interface or the VideoLAN Website.

### Report bugs
If you found a possible bug in VLC or want to propose a new feature or enhancement,
then check out our [bug reporting guidelines]({% link docs/intro/bugreports.md %}).


## Get started
Sounds interesting? The best way to get started is to first check out our
introduction, as it contains a high-level overview of how to get in touch with
other developers to coordinate, fetch the VLC source code and contribute your
changes back.

[Get started]({% link docs/intro/index.md %}){: .btn .btn-primary .my-2 }
{: .text-center }
