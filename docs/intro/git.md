---
title: Git Guide
permalink: /docs/intro/git
layout: default

nav_order: 2
parent: Introduction
---

# Git Guide

Git is a free [version control system][vcs] (VCS), used by programmers to keep track of the different versions of the
files composing a software. Some basic understanding of how it works is needed, in order to make changes to the
source code of the VideoLAN projects and contribute those changes back.

This page attempts to give a brief introduction and some useful tips about Git, but it can't give you a full
guide on how to use Git (especially if you've never used a VCS before) or be a complete reference. You can
check out the following useful references if you are unfamiliar with Git:

- [git - the simple guide](https://rogerdudler.github.io/git-guide/)
- [Git SCM Docs (official documentation)](https://git-scm.com/doc)

## Basic usage
First, you need to check out the source code from the VideoLAN Git using the <kbd>git clone</kbd> command.
Depending for which project you want to obtain the source code, the address will be different. Below is an
example for how to checkout the VLC development sources:

<samp>$ <kbd>git clone https://git.videolan.org/git/vlc.git</kbd></samp>

Voilà! The full VLC source code and version history should be on your hard disk in the <samp>vlc</samp> folder.

## Configure Git
When contributing to VLC and other VideoLAN projects, you should make sure you have your correct
name and e-mail address set. In case you have not configured them yet, you can do so with:

<samp>$ <kbd>git config --global user.name "Your Name"</kbd></samp>  
<samp>$ <kbd>git config --global user.email "me@example.com"</kbd></samp>

Alternatively if you only want to set those for the current repository, for example
because you want just your VLC commits to use your work e-mail, just omit the
`--global` flag.

Additionally there are some useful git aliases you can configure, which make
some common git tasks easier:

### Tip: Setting up <kbd>git up</kbd>
If you want to be able to just keep in sync using <kbd>git up</kbd> use:

<samp>$ <kbd>git config --global alias.up "pull --rebase"</kbd></samp>

And if you like your tree to be messy and don't want git to complain (like with SVN) use:

<samp>$ <kbd>git config --global alias.up '!sh -c "git commit -a -m "Before rebase" && git pull --rebase && git reset head^"'</kbd></samp>

### Tip: Setting up <kbd>git wu</kbd>
If you want to see what you are about to <kbd>git push</kbd> you can set up the
wup (Git, what's up?) alias for that:

<samp>$ <kbd>git config --global alias.wu "log --stat origin..@{0}"</kbd></samp>

### Tip: Setting up <kbd>git wup</kbd>
This is quite similar to <kbd>git wu</kbd>, but additionally it shows the
diff along with the commits.

<samp>$ <kbd>git config --global alias.wup "log -p origin..@{0}"</kbd></samp>

## Quick fixes
Sometimes, when you just want to make a small change, like fix a typo or so,
it might not be worth it to create a branch for it. In that case just work on the
master branch and commit your changes. Once there are changes on the remote, you can
easily rebase using:

<samp>$ <kbd>git pull --rebase</kbd></samp>

Or, if you configured the <kbd>up</kbd> alias mentioned above, just:

<samp>$ <kbd>git up</kbd></samp>

## Working on a feature/bugfix
Let's say now you want to work on a new feature or fix a bug you found. The best way to do that is to
first create a local branch for that:

<samp>$ <kbd>git checkout -b add-foo-codec</kbd></samp>

Now do your work on the newly created branch. If meanwhile there were more changes made on the remote,
you might need to rebase your branch:

<samp>$ <kbd>git fetch origin</kbd></samp>  
<samp>$ <kbd>git rebase origin/master</kbd></samp>

[vcs]: http://en.wikipedia.org/wiki/Revision_control
