---
title: Reporting Bugs
permalink: /docs/intro/bugreports
layout: default

nav_order: 1
parent: Introduction
---

# Reporting Bugs
{: .fs-9 }

If you find any bugs, or if you experience any crashes, then we would very much
like to hear about the issue. This page covers how to report a bug and which
information you need to provide for a bug report to be useful for us.
{: .fs-6 .fw-300 }

## Is it truly a new bug?
It may seem silly, but a large proportion of the submitted bug reports are not
really bugs at all, or have been known for months, and/or are already fixed.

To avoid that, please check the following:

- **Version:**  
  Are you really using the latest stable release of VLC?
  You can find the most recent version on our [Website][vlc].

- **Preferences:**  
  Have you tried _resetting_ your preferences and restarting VLC yet?
  Check [this page][reset-prefs] for how to do that.

- **Nightly builds:**  
  Before you file a new bug, please try a preview development build of our
  next version on our [nightly builds][nightlies] website.
  The bug may already be fixed in those builds.

- **Closed bugs:**  
  Many bugs are already known. Sometimes they are already fixed for the next
  version of VLC, see the nightly builds section above.
  Please [search][trac] the list of known and fixed issues.
  If you find your issue, check if it is already closed.

- **New Features:**  
  Some things are simply not yet supported by VLC.
  We have a [list of things][trac-features-query] we would like to implement in
  the future. If your suggestion is not on this list, you might want to make a
  _feature request_ ticket on trac.


If you checked all the things above and none of them ruled out your bug,
you can report it in your bug tracker.


## Sending a bug report
When you create a new bug report, include as much information as possible
to help us to understand and reproduce your problem. We try to answer all
your reports, but there are so many that we simply do not have the time to
do so for everyone.

The more sound and concrete details you provide about your issue,
the better the chances that we will investigate it fully.

### Information to submit
Please make sure to include the following information in your report:

- **Operating system:**  
  The name of your operating system and the precise version
  (include the Linux distribution or the version of Windows).

- **VLC version:**  
  Include the version number of the VLC version that you reproduce the bug with.
  In case it is a nightly, please provide the full version information string,
  like: <samp>4.0.0-dev Otto Chriek (revision 4.0.0-dev-726-g63c21a090b)</samp>.

- **Hardware:**  
  Make sure to name all the audio and video hardware in question that might
  be related to VLC's usage. For instance:
  <samp>MacBook Pro (Retina, 13", Fall 2013) with Intel Iris 1536 MB,
  with an external USB dolby surround output device.</samp>

- **Logs:**  
  Complete logs as those from the messages dialog, verbosity set
  to debug (GUI) or the <code>-vvv</code> switch (CLI mode).

- **Steps to reproduce:**  
  Please describe the steps to reproduce your problem, be as precise
  as possible. If you are uncomfortable with words, you may also use
  screenshots.

- **Reproducibility:**  
  Is your problem always reproducible?  Does it occur every time? If not, how often?

- **File/Format:**  
  If applicable, the type (movie, music, subtitle) and the format of the file or
  stream being played. If known, please also list the codec(s) in the file.

- **Sample file:**  
  If the issue is specific to a certain file, providing us this file is always very
  helpful, if not indispensible. In this case, please upload as much of the file as
  you can to our [samples server][streams-upload].


### Restrictions

**If your problem is not a bug** but instead a support question,
please **do not file a report for it.** Instead use the [Forum][vlc-forum]
to ask your question!

**Bugs with insufficient infos may be closed as incomplete** (or invalid in the worst cases).
VLC developers understand that the requested infos can be difficult to gather or provide.
Sharing sample files can pose confidentiality, privacy or piracy issues. However as a bug
reporter you have to accept that the VLC developers cannot resolve bugs without sufficient
infos for analysis.

**Please keep in mind that the VLC developers are not paid to solve your problems.**
There cannot be any warranty for any kind of timeline regarding resolution of a bug. On the one
hand, some bugs have been fixed within minutes of being filed. On the other hand, some bugs have
remained open for over a decade, with no signs of getting fixed any time soon. If you need express
resolution, you should attempt to solve the problem yourself, or consider hiring a software consultant.

[Create ticket][trac-newticket]{: .btn .btn-primary .my-2 }  
Trac account required, if you do not have one, [register here][trac-register].
{: .text-center }

---

### Crash bugs

If your bug is about VLC crashing, we usually need additional information to be
able to solve your problem!

On **Windows** and **macOS** since VLC 3.0 we include a crash reporter,
which should prompt you to upload information about the crash that happened to
VideoLAN. In this case, when you file a bug about that crash, include the time
when the crash happened, that makes it easier to figure out which report could
belong to the bug you describe.

If you are on **Linux** we need a full symbolicated stack trace if you
report a crash bug to us. Without that, it is really hard to figure out the cause
of your bug, unless it is easily reproducible.
If we are unable to reproduce and you can't provide a stack trace, the issue is
likely to get closed!


[vlc]: https://www.videolan.org/vlc/
[reset-prefs]: https://wiki.videolan.org/VSG:ResetPrefs/
[nightlies]: https://nightlies.videolan.org
[trac]: https://trac.videolan.org/vlc
[trac-newticket]: https://trac.videolan.org/vlc/newticket
[trac-register]: https://trac.videolan.org/vlc/register
[trac-features-query]: https://trac.videolan.org/vlc/query?action=view&type=enhancement&order=priority
[streams-upload]: http://streams.videolan.org/upload/
[vlc-forum]: https://forum.videolan.org
