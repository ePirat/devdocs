---
title: Introduction
permalink: /docs/intro/
layout: default

nav_order: 2
has_children: true
has_toc: false
---

# Introduction
{: .fs-9 }

This is a quick introduction into VLC development, how to communicate and coordinate with the
other developers, get the source code and how to submit your changes back.
{: .fs-6 .fw-300 }

## Communicating
The most important part is how to communicate with other VLC developers.

### Chat (IRC)
A lot of communication happens via Internet Relay Chat (IRC) on the _#videolan_
chatroom (“channel”) on [Freenode][freenode]. To join the chat you
can either use a IRC client program or the Webchat. The recommended way is to
use a IRC client program as it makes it easier to stay connected and online in
the chat for longer time.

Popular clients are:
- [HexChat](https://hexchat.github.io) (Windows/Linux/macOS)
- [Quassel IRC](https://quassel-irc.org) (Windows/Linux/macOS)
- [Textual](https://www.codeux.com/textual/) (macOS)

[Open Webchat][freenode-webchat]{: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }
[Direct IRC Link][irc-direct]{: .btn .fs-5 .mb-4 .mb-md-0 }

### Mailinglist (ML)
For longer discussions and to follow the overall development, there is
the [vlc-devel][vlc-devel] mailinglist.
The mailing list is mostly used for submissions of source code patches and
review and discussion of these patches. Additionally important announcements
are made on this list, so it is strongly recommended to subscribe to that list.

All emails to that list are [publicly archived][vlc-devel-archive] and can be
browser using a web interface. Patches sent to that list will be additionally
available on [Patchwork][patchwork].

[Subscribe the List][vlc-devel]{: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }
[Browse archives][vlc-devel-archive]{: .btn .fs-5 .mb-4 .mb-md-0 }

### Bugtracker (Trac)
To keep track of all Bugreports and to plan Milestones, VLC currently has a
[Trac instance][trac].
There tickets can be created for bugs or feature requests, and it is a good
place to find things to work on.

Before filing new bugs, make sure you've read the bug report guidelines
which detail how to write a useful report and which information need
to be in it and how to obtain them.

[Report Guide]({% link docs/intro/bugreports.md %}){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }
[Browse Tickets][trac-report]{: .btn .fs-5 .mb-4 .mb-md-0 }

----

## Getting the Source
If you want to contribute code to VLC, you need to get familiar with how to
get the source code. All VideoLAN projects use the [Git SCM][git-scm] for their
code. In case you are not familiar with git yet, make sure you get a basic
understanding about how Git works before you continue, as this is essential
and not covered by this developer documentation.

Most VideoLAN projects are nowadays hosted at the
[VideoLAN GitLab][videolan-gitlab] instance, some projects, noticeably VLC
itself is still hosted on the old Git server at `git.videolan.org`
and can be browsed using the [Gitweb interface][videolan-gitweb].
For more information about the Git workflow for VLC check out the
dedicated Git page.

[Git Guide]({% link docs/intro/git.md %}){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }

## Submitting patches
Once you've worked on a feature or fixed a bug, to submit your code back you
need to send a patch to the mailinglist. The easiest way to do this is using
the <code><strong>git</strong> format-patch</code>
and <code><strong>git</strong> send-email</code> commands.
More details how to use them and which conventions we expect can be found
on the “Submitting patches” page.

[Submitting patches]({% link docs/intro/index.md %}){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }


[freenode]: https://freenode.net
[freenode-webchat]: http://webchat.freenode.net/?channels=%23videolan
[irc-direct]: irc://freenode.net:6667/#videolan
[vlc-devel]: https://mailman.videolan.org/listinfo/vlc-devel
[vlc-devel-archive]: https://mailman.videolan.org/pipermail/vlc-devel/
[patchwork]: https://patches.videolan.org
[trac]: https://trac.videolan.org/vlc
[trac-report]: https://trac.videolan.org/vlc/report/3
[git-scm]: https://git-scm.com
[videolan-gitlab]: https://code.videolan.org
[videolan-gitweb]: https://git.videolan.org/
